max_num = int(input("Bitte geben Sie die maximale Zahl ein: "))

def ist_primzahl(n):
    if n <= 1:
        return False
    if n <= 3:
        return True
    if n % 2 == 0 or n % 3 == 0:
        return False
    i = 5
    while i * i <= n:
        if n % i == 0 or n % (i + 2) == 0:
            return False
        i += 6
    return True

print("Primzahlen bis", max_num, "sind:")
for num in range(2, max_num + 1):
    if ist_primzahl(num):
        print(num)
